var asideTag = document.getElementsByTagName('aside');
console.log("Identity of the tag containing the navigation: " + asideTag);

// Apparently getElementsByTagName returns some kind of array-ish thing, so "[0]"
var asideHeight = asideTag[0].style.height;
console.log("Height that needs to be cleared: " + asideHeight);

var headerTag = document.getElementsByTagName('header');
// Should this be written instead in a way that will allow more than one <article> in the HTML?
var articleTag = document.getElementsByTagName('article');
var footerTag = document.getElementsByTagName('footer');

var pushedTags = [headerTag, articleTag, footerTag];
console.log("Number of HTML tags to be pushed down (1 added to take it out of base 0): " + pushedTags.length++);
pushedTags.forEach(function(item, index, array) {
	console.log(item, index);
})

document.onreadystatechange = function() {
	// if (document.readyState === 'complete') {
	// 	displacementValue = '1000px';
	// }
}